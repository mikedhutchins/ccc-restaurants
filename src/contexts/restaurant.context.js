import React, { createContext, useEffect, useState } from "react";
import { getRestaurants, track } from "../services/restaurants.service";
import ReactDOM from 'react-dom';
import { prepareList } from "../transforms/list.utility";
import { mapRestaurants } from "../transforms/restaurant.map";

export const RestaurantContext = createContext();

const RestaurantProvider = ({ children }) => {
    const [items, setItems] = useState();
    const [filters, setFilters] = useState([]);
    useEffect(() => { 
        const get = async () => { 
            return await getRestaurants();
        }
        if (!items) {
            track();
            get()
                .then(response => prepareList(response, ['state', 'genre', 'tags', 'attire']))
                .then(response => {
                    const { list, filters } = response; 
                    ReactDOM.unstable_batchedUpdates(() => { 
                        setFilters(filters);
                        setItems(mapRestaurants(list));
                    })
                });
        }
    });

    const values = { 
        items,
        filters
    } 
    return <RestaurantContext.Provider value={values}>
        {items ? children : <></>}
    </RestaurantContext.Provider>;
}

export default RestaurantProvider;