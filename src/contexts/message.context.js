import React, { createContext } from 'react';
import ReactDOM from 'react-dom';
import { useState } from 'react';
import ModalForm from '../components/common/modal.component';

export const MessageContext = createContext({});

export const MessageProvider = ({ children }) => {
    const [Modal, setModal] = useState(() => <></>);
    const [showModal, setShowModal] = useState(false);
    const startModal = (comp) => {
        ReactDOM.unstable_batchedUpdates(() => {
            setModal(comp);
            setShowModal(true);
        });
    };
    const closeModal = () => {
        ReactDOM.unstable_batchedUpdates(() => {
            setModal(() => <></>);
            setShowModal(false);
        });
    };

    const values = {
        startModal,
        closeModal,
        Modal,
    };

    return <MessageContext.Provider value={values}>
        {children}
        {showModal ? <ModalForm /> : <></>}
    </MessageContext.Provider>
};