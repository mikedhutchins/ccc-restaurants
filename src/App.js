import './App.scss';
import RestaurantList from './components/restaurant-list.component';
import { MessageProvider } from './contexts/message.context';
import RestaurantProvider from './contexts/restaurant.context';

function App() {
  return (
      <RestaurantProvider>
        <RestaurantList />
      </RestaurantProvider>
    );
}

export default App;
