import React, { useContext } from 'react';
import { ListContext } from './list.context';

const ListBody = ({ row }) => { 
    const { refined } = useContext(ListContext);

    return <div className="list-container text-center">
        
        {refined.map(item => <div key={item.id} className="card m-3">{row(item)}</div>)}
    </div>;
}

export default ListBody;