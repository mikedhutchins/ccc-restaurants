import React, { useContext, useEffect, useState } from 'react';
import { MessageContext } from '../../contexts/message.context';
import { ListContext } from './list.context';

const FilterValue = ({ filter: { name }, value }) => { 
    const { setFilter } = useContext(ListContext);

    return <li className={["filter-value", value.selected ? "selected" : ''].join(' ')} onClick={e => setFilter(name, value )}>
        {value.value}
    </li>
}

const FilterModal = ({ onContinue, filter, filter: { name } }) => {
    const { filters } = useContext(ListContext);
    const [nFilter] = filters.filter(x => x.name === name);
    const { values } = nFilter;
    return <div className="filters expand p-3">
        <ul className="filter-section">
            <h4>Filter by {name}</h4>
            {values.map(val => <FilterValue key={val.value} filter={nFilter} value={val} />)}
        </ul>
        <hr></hr>
        <p style={{ textAlign: 'center' }}>
            <button className="button button-primary" style={{ margin: 5 }} onClick={onContinue}>Close</button>
        </p>
    </div>
};

const ListFilters = ({ visible }) => { 
    const { filters, clearAll, setSearch, searchCriteria, sorts, setSort } = useContext(ListContext);
    const { startModal, closeModal } = useContext(MessageContext);
    const [criteria, setCriteria] = useState(searchCriteria);

    useEffect(() => { 
        setCriteria(searchCriteria);
    }, [searchCriteria]);

    const attemptSearch = (key) => { 
        if (key==='Enter' && criteria.length > 0) { 
            setSearch(criteria);
        }
        if (criteria.length === 0) { 
            setSearch('');
        }
    }
    const showFilter = (filter) => { 
        startModal(() => <FilterModal filter={filter} onContinue={() => {
            closeModal();
        }} ></FilterModal>)
    }

    return <div className={['filters', 'm-5', visible ? 'expand' : ''].join(' ')}>
        <div className="m-4">
            {filters.map(filter => 
                <button key={filter.name}
                    className="button button-primary m-2 p-2" 
                    onClick={e => showFilter(filter)}>
                        Filter by {filter.name} 
                </button>)}
                <button
                    className="button button-primary m-2 p-2"  onClick={clearAll}
                    >Clear All</button>
            <hr></hr>
            {sorts.map(sort => 
                <button key={sort} className="button button-primary m-2 p-2" onClick={e => setSort(sort)}>
                    Sort by {sort}
                </button>)}
        </div>
        <hr />
        <div className="m-4">
            <input className="search-box" value={criteria} onChange={e => setCriteria(e.currentTarget.value)} onKeyUp={e => attemptSearch(e.key)} placeholder="Search here"></input>
        </div>
    </div>;
}

export default ListFilters;
