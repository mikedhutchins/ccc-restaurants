import React, { useContext } from 'react';
import { MessageContext } from '../../contexts/message.context';

const ModalForm = () => {
    const { Modal } = useContext(MessageContext);

    return <div className="modal-bg">
        <div className="modal-form">
            {Modal}
        </div>
    </div>
};

export default ModalForm;