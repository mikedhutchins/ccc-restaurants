import React, { useContext } from 'react';
import { ListContext } from './list.context';

const ListFooter = () => { 
    const { nextPage, totalCount, foundCount, refinedCount, previousPage, canGoBack, canAdvance, page: { listSize, pages, currentPage }, setListSize } = useContext(ListContext)
    return <div className="footer">
        <h2 className="m-3">
            Found {foundCount} restaurants. displaying {refinedCount}.{' '}
            {pages === pages ? <>Page #{currentPage + 1} of {pages}</> : <></>}
        </h2>

        {pages > 1 ? <button className="button button-primary m-2" onClick={previousPage} disabled={!canGoBack}>Previous Page</button> : <></>}

        <select className="button button-primary  m-2" value={listSize} onChange={e => setListSize(parseInt(e.currentTarget.value))}>
            <option value="10">10</option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="100">All</option>
        </select>
        {pages > 1 ? <button className="button button-primary m-2" onClick={nextPage} disabled={!canAdvance}>Next Page</button> : <></>}
        
    </div>;
}

export default ListFooter;