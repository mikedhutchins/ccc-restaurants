import React, { createContext, useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import { MessageProvider } from '../../contexts/message.context';

export const ListContext = createContext();

export const pageFn = (currentPage, listSize) => { 
    return (i, index) => { 
        const currentIndex = (currentPage*listSize);
        return index >= currentIndex && index < (currentIndex+listSize);    
    }
};

const List = ({ children, items, filters, sorts }) => { 
    const totalCount = items.length;
    const [page, setPage] = useState({ currentPage: 0, listSize: 10, pages: 0 });
    const [refinedCount, setRefinedCount] = useState(items.length);
    const [foundCount, setFoundCount] = useState(items.length);
    const [refined, setRefined] = useState(items);
    const [internalFilters, setFilters] = useState(filters);
    const [needsRefining, setNeedsRefining] = useState(true);
    const [searchCriteria, setSearchCriteria] = useState('');
    const [sortBy, setSortBy] = useState(sorts[0]);
    const [direction, setDirection] = useState('asc');
    
    const resetRefined = (fn) => { 
        ReactDOM.unstable_batchedUpdates(() => { 
            fn();
            setNeedsRefining(true);
        });
    }

    const setSort = (sort) => 
        resetRefined(() => { 
            setDirection(sort === sortBy && direction === "asc" ? 'desc' : 'asc');
            setSortBy(sort); 
        });

    const setSearch = (search) => 
        resetRefined(() => setSearchCriteria(search));

    const setListSize = (size) => 
        resetRefined(() => setPage({ ...page, listSize: size }));

    const nextPage = () =>  
        resetRefined(() => setPage({ ...page, currentPage: page.currentPage + 1 }));
        ;

    const previousPage = () => 
        resetRefined(() => setPage({ ...page, currentPage: page.currentPage - 1 }))
        ;

    const clearAll = () => 
        resetRefined(() => { 
            setSearchCriteria('');
            setFilters(internalFilters.map(filter => ({ ...filter, values: [...filter.values.map(v => ({...v, selected: v.isAll }))]})));
        });

        const setFilter = (filterName, value) => {
        const filterFn = (filter) => { 
            if (filter.name === filterName) { 
                if (value.isAll) { 
                    return { 
                        ...filter,
                        values: filter.values.map(v => ({ ...v, selected: v.isAll }))
                    };
                }
                let values = filter.values
                    .map(v => ({...v, selected: v.isAll ? false : (v.value === value.value ? !v.selected : v.selected)}));
                if (values.filter(x => x.selected).length === 0) { 
                    values = [...filter.values.map(v => ({ ...v, selected: v.isAll }))];
                }
                return { 
                    ...filter, 
                    values
                }
            }
            return {...filter};
        }
        resetRefined(() => setFilters([...internalFilters.map(filterFn)]));
    }

    const canAdvance = page.currentPage < (page.pages - 1);
    const canGoBack = page.currentPage > 0;


    useEffect(() => { 
            const refineByFilter = (item) => 
                internalFilters
                    .filter(filter => filter.values.filter(x => 
                        (x.isAll && x.selected) ||
                        (x.selected && item[filter.name].indexOf(x.value) >= 0)).length > 0).length >= internalFilters.length;

            const refineBySearch = (item) => 
                        searchCriteria.length === 0 || item.search.indexOf(searchCriteria) >= 0;

            const refine = () => { 
            ReactDOM.unstable_batchedUpdates(() => { 
                let sub = items
                    .filter(refineByFilter)
                    .filter(refineBySearch)
                    .sort((a, b) => direction === 'asc' ? b[sortBy] > a[sortBy] ? -1 : 1 : a[sortBy] > b[sortBy] ? -1 : 1)
                    ;
                const filtered = sub.length;
                setFoundCount(filtered);
                const pages = page.pages;
                const totalPages = Math.ceil(filtered / page.listSize);
                const currentPage = totalPages < pages ? 0 : page.currentPage;
                
                sub = sub
                    .filter(pageFn(currentPage, page.listSize))
                    ;
                setRefinedCount(sub.length);
                setRefined(sub);
                setPage({ ...page, pages: Math.ceil(filtered / page.listSize), currentPage});
                setNeedsRefining(false);
            })
            ;
        }
        if(needsRefining) { 
            refine();
        }
    }, [items, page.currentPage, page.currentIndex, page.listSize, page, needsRefining, internalFilters, refined.length, searchCriteria, direction, sortBy]);


    const values = { 
        items,
        filters: internalFilters,
        totalCount,
        refinedCount,
        refined,
        foundCount,
        page,
        searchCriteria,
        setSearch,
        setListSize,
        nextPage, 
        previousPage,
        setFilter,
        clearAll,
        sortBy,
        sorts,
        setSort,
        canAdvance,
        canGoBack,
        needsRefining,
    };
    return <ListContext.Provider value={values}>        
        <MessageProvider>
            {children}
        </MessageProvider>
    </ListContext.Provider>;
};

export default List;