import React, { useContext, useEffect, useState } from 'react';
import { ListContext } from './list.context';
import ListFilters from './list.filters.component';

const ListHeader = () => { 
    const [toggle, setToggle] = useState(false);
    const { needsRefining } = useContext(ListContext);

    useEffect(() => { 
        setToggle(false);
    }, [needsRefining]);
    return <div className="header">
        <div className="menu fa fa-bars" onClick={e => setToggle(!toggle)}></div>
        <h1 className="m-2">
            Hungry? Let us help you find your next foodie experience!
        </h1>
        <ListFilters visible={toggle}/>

    </div>;
}

export default ListHeader;