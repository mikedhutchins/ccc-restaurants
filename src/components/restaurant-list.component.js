import React, { useContext, useState } from 'react';
import {RestaurantContext} from '../contexts/restaurant.context';
import ListBody from './common/list.body.ccomponent';
import List from './common/list.context';
import ListFooter from './common/list.footer.component';
import ListHeader from './common/list.header.component';

const Row = ({website, address1, city, state, attire, telephone, name, genre, zip, hours }) => { 
    const [more, setMore] = useState(false);
    return <div onClick={e => setMore(!more)}>
    <div className="text-left">
        <a href={website} target={name} onClick={e => e.stopPropagation()}>{name}</a>{" "}
        <a href={`tel:${telephone}`} onClick={e => e.stopPropagation()} className="phone">Call for reservations: {telephone}</a>
    </div>
    <div  className="text-left">
        {address1}, {city}, {state} {zip}
    </div>
    {more && <div>
        <div>Attire: {attire}</div>
        <div>Hours: {hours}</div>
        </div>}
    <div className="m-3 text-right">
        {genre.split(',').map(g => <strong key={g} className="genre">{g}</strong>)}
    </div>

</div>;
}

const RestaurantList = () => { 
    const { items, filters } = useContext(RestaurantContext);
    const row = (item) => { 
        return <Row {...item} />;
    };
    return <List items={items} filters={filters} sorts={['name', 'state']}>
        <ListHeader />
        <ListBody row={row} />
        <ListFooter />

    </List>;
};

export default RestaurantList;
