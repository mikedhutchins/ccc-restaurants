import { getUniques } from "./list.utility";
const list = [
    {
        category: 'three'
    },
    {
        category: 'one'
    },
    {
        category: 'one'
    },
    {
        category: 'two'
    }
];

test('given 3 unique values in list a list of 4 items getUniques returns 3 unique items', () => { 
    const uniques = getUniques(list, 'category');
    expect(uniques.length).toEqual(3);
});

test('given 3 unique values in list with one duplicated getUniques returns 3 unique items sorted with most frequent at top', () => { 
    const uniques = getUniques(list, 'category');
    expect(uniques[0].value).toEqual('one');
});

test('given 3 unique values in list with one duplicated getUniques returns 3 unique items sorted with top count', () => { 
    const uniques = getUniques(list, 'category');
    expect(uniques[0].count).toEqual(2);
});