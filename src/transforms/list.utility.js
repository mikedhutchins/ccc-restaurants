export const getUniques = (list, property) => { 
    let values = { };
    let isList = false;
    if (list[0][property].indexOf(',') >= 0) { 
        list.forEach(item => { 
            item[property].split(',').forEach(i => { 
                values = {
                    ...values, 
                    [i]: (values[i] ?? 0) + 1
                }
            });
        });
        isList = true;
    }
    else { 
        list.forEach(item => 
            values = {
            ...values, 
            [item[property]]: (values[item[property]] ?? 0) + 1 
        });
    }
    return [{ value: 'all', selected: true, isAll: true }, ...Object.keys(values)
        .map(value => ({
            value,
            count: values[value],
            isList
        }))
        .sort((a, b) => b.count - a.count)];
};

export const prepareList = (list, filters) => { 
    const sets = (filters ?? [])
        .map(name => (
            {
                name,
                values: getUniques(list, name)
            }
        ))
        ;
    
    return { 
        list,
        filters: sets
    };
}