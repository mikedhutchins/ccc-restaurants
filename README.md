# Thanks for looking at my Coding Challenge Submission

## I believe that I was able to complete all of the requirements and some of the stretch goals.

## Github vs. Gitlab

I'm fairly sure that Charter / Spectrum still uses Gitlab and I haven't used
Github in quite some time. If this is an issue, please contact me at
mike@sangremadera.com and I'll create a new account, and push.

## Not a lot of time.

I completed this over the weekend and that inbetween meetings and a
_spectacular_ Tampa win. In general, my current position keeps me quite busy
during the week. I had fun completing the challenge, but cheated just a bit in
sort of half-heartedly using TailwindCSS, font awesome, and only writing a
single UT.

IT works in mobile fyi.

## Pipeline

This is deployed here
https://cccrestaurants.s3-us-west-1.amazonaws.com/index.html

# Again Thank you for considering my coding challenge!
